import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription} from 'rxjs';
import { TasksService } from '../tasks.service';
import { Task } from '../task';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  private task: Task;

  postTask(text: string): void {
    text = text.trim();
    if (!text) { return; }
    this.task = {text, date: new Date()};
    this.tasksService.addTask(this.task)
      .subscribe(task => this.tasksService.pushTask(task));

  }
  constructor(private tasksService: TasksService) {
  }

  ngOnInit() {
  }

}
