import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ModalComponent } from '../modal/modal.component';
import { ModalEditComponent } from '../modal-edit/modal-edit.component';
import { Task } from '../task';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-tablelist',
  templateUrl: './tablelist.component.html',
  styleUrls: ['./tablelist.component.css']
})
export class TablelistComponent implements OnInit {
  displayedColumns: string[] = ['id', 'text', 'date', 'actions'];
  dataSource: Task[];
  task: Task;
  @Output() openEvent = new EventEmitter();
  constructor(public dialog: MatDialog, public dialogEdit: MatDialog, private tasksService: TasksService) {
    this.tasksService.taskPush$.subscribe(
      task => this.dataSource.push(task)
    );
  }

  getSource(): void {
    this.tasksService.getTasks()
      .subscribe(tasks => this.dataSource = tasks);
  }

  openDialog(id): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      data: {
        id
      },
      width: '250px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.tasksService.removeTask(result)
        .subscribe(task => console.log(task));
      this.dataSource = this.dataSource.filter(x => x.id !== result.id);
    });
  }
  openEditDialog(id: number, text: string): void {
    const dialogEditRef = this.dialogEdit.open(ModalEditComponent, {
      data: {
        id,
        text
      },
      width: '350px',
    });
    dialogEditRef.afterClosed().subscribe(result => {
      for (let i = 0; i <= this.dataSource.length; i++) {
        if (this.dataSource[i].id === result.id) {
          this.dataSource[i].text = result.text;
        }
      }
      this.tasksService.updateTask(result).subscribe(task => console.log(task));
    });
  }
  ngOnInit() {
    this.getSource();
  }
  toggleDrawer() {
    this.openEvent.emit();
  }
}
