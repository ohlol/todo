import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Task } from './task';


@Injectable({
  providedIn: 'root'
})
export class TasksService {
  private tasksUrl = 'http://api';

  private taskPushSource = new Subject<Task>();

  taskPush$ = this.taskPushSource.asObservable();

  pushTask(task: Task) {
    this.taskPushSource.next(task);
  }

  constructor(private http: HttpClient) { }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.tasksUrl}/read.php`);
  }
  addTask (text: Task): Observable<Task> {
    return this.http.post<Task>(`${this.tasksUrl}/create.php`, JSON.stringify(text));
  }
  removeTask(task: Task): Observable<Task[]> {
    return this.http.post<Task[]>(`${this.tasksUrl}/delete.php`, JSON.stringify(task));
  }
  updateTask(task: Task): Observable<Task[]> {
    return this.http.post<Task[]>(`${this.tasksUrl}/update.php`, JSON.stringify(task));
  }
}
