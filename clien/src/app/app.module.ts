import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatDialogModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';

import { AppComponent } from './app.component';
import { TablelistComponent } from './tablelist/tablelist.component';
import { FormComponent } from './form/form.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalComponent } from './modal/modal.component';
import { ModalEditComponent } from './modal-edit/modal-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    TablelistComponent,
    FormComponent,
    ModalComponent,
    ModalEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatTableModule,
    CdkTableModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatDialogModule
  ],
  entryComponents: [ModalComponent, ModalEditComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
