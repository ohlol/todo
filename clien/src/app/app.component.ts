import { Component, ViewChild } from '@angular/core';
import { TablelistComponent } from './tablelist/tablelist.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'novaleads';
  events = [];
  opened = false;
  openEvent() {
    this.opened = !this.opened;
  }
  constructor() {}
}
