<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include_once './database.php';
include_once './task.php';
 
$database = new Database();
$db = $database->getConnection();
 
$task = new Task($db);

$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data->text) &&
    !empty($data->date)
){

    $task->text = $data->text;
    $task->date = $data->date;

    if($task->create()){

        http_response_code(201);

        echo json_encode(["message" => "task was created."]);
    }

    else{

        http_response_code(503);

        echo json_encode(["message" => "Unable to create product."]);
    }
}

else{

    http_response_code(400);

    echo json_encode(["message" => "Unable to create task. Data is incomplete."]);
}