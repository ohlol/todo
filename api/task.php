<?php
class Task{
 
    private $conn;
    private $table_name = "tasks";
 
    public $id;
    public $text;
    public $date;

    public function __construct($db){
        $this->conn = $db;
    }
    public function read(){

      $query = "SELECT * FROM tasks";

      $stmt = $this->conn->prepare($query);

      $stmt->execute();

      return $stmt;
    }
    public function create(){

        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    text=:text, date=:date";

        $stmt = $this->conn->prepare($query);

        $this->text=htmlspecialchars(strip_tags($this->text));
        $this->date=htmlspecialchars(strip_tags($this->date));

        $stmt->bindParam(":text", $this->text);
        $stmt->bindParam(":date", $this->date);

        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }

    public function update(){

      $query = "UPDATE
                  " . $this->table_name . "
              SET
                  text = :text
              WHERE
                  id = :id";

      $stmt = $this->conn->prepare($query);

      $this->text=htmlspecialchars(strip_tags($this->text));
      $this->id=htmlspecialchars(strip_tags($this->id));

      $stmt->bindParam(':text', $this->text);
      $stmt->bindParam(':id', $this->id);

      if($stmt->execute()){
          return true;
      }

      return false;
    }
    public function delete(){
      $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
   
      $stmt = $this->conn->prepare($query);

      $this->id=htmlspecialchars(strip_tags($this->id));

      $stmt->bindParam(1, $this->id);

      if($stmt->execute()){
          return true;
      }
   
      return false;
       
  }
}