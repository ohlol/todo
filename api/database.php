<?php
class Database{

    private $host = "localhost";
    private $db_name = "todo";
    private $username = "root";
    private $password = "";
    public $conn;

    public function getConnection(){
        $table = "tasks";
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
            $queryCreateTable = "CREATE TABLE IF NOT EXISTS $table (
              id int(11) NOT NULL AUTO_INCREMENT,
              text text NOT NULL,
              date datetime NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;";
            $this->conn->exec($queryCreateTable);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}


