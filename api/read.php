<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './database.php';
include_once './task.php';

$database = new Database();
$db = $database->getConnection();

$task = new Task($db);

$stmt = $task->read();
$num = $stmt->rowCount();

if($num>0){

    $tasks_arr=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
 
        $task_item=[
            "id" => $id,
            "text" => $text,
            "date" => $date
        ];
 
        array_push($tasks_arr, $task_item);
    }

    http_response_code(200);

    echo json_encode($tasks_arr);
} else{
 
  http_response_code(404);

  echo json_encode(
      array("message" => "No tasks found.")
  );
}
 